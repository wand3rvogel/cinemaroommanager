package cinema;

import java.util.Arrays;
import java.util.Scanner;

public class CinemaRoomManager {

    private static char[][] seats;
    private static int purchasedTickets = 0;
    private static int currentIncome = 0;
    private static final Scanner scanner = new Scanner(System.in);
    private static final int LARGE_LIMIT = 60;
    private static final int NORMAL_PRICE = 10;
    private static final int CUT_PRICE = 8;



    public static void main(String[] args) {

        createCinema();

        int choice;
        do {
            choice = getAction();
            switch (choice) {
                case 1:
                    printCinema();
                    break;
                case 2:
                    buyTicket();
                    break;
                case 3:
                    printStatistics();
                    break;
            }
        } while (choice != 0);
    }

    private static void printStatistics() {

        System.out.printf("%nNumber of purchased tickets: %d%n", purchasedTickets);

        double percentPurchased = (double) purchasedTickets / (seats.length * seats[0].length) * 100;
        System.out.printf("Percentage: %.2f%%%n", percentPurchased);

        System.out.printf("Current income: $%d%n", currentIncome);

        System.out.printf("Total income: $%d%n", calculateProfit());
    }

    private static void buyTicket() {

        boolean isBought = false;

        do {
            System.out.println("\nEnter a row number:");
            int seatRow = scanner.nextInt();
            System.out.println("Enter a seat number in that row:");
            int seatCol = scanner.nextInt();

            if (seatRow < 1 || seatRow > seats.length || seatCol < 1 || seatCol > seats[0].length) {
                System.out.println("\nWrong input!");
                continue;
            }

            if (seats[seatRow-1][seatCol-1] == 'B') {
                System.out.println("\nThat ticket has already been purchased!");
                continue;
            }

            bookSeat(seatRow, seatCol);
            isBought = true;

        } while (!isBought);
    }

    private static int getAction() {

        String[] actions = {
                "Show the seats",
                "Buy a ticket",
                "Statistics"
        };

        System.out.println();
        for (int i = 0; i < actions.length; i++) {
            System.out.printf("%d. %s%n", i + 1, actions[i]);
        }
        System.out.println("0. Exit");

        return scanner.nextInt();
    }

    private static void bookSeat(int row, int col) {

        seats[row-1][col-1] = 'B';

        int seatPrice = getPriceForSeat(row);
        System.out.printf("\nTicket price: $%d%n", seatPrice);
        purchasedTickets++;
        currentIncome += seatPrice;
    }

    private static int getPriceForSeat(int row) {

        int result;
        int totalSeats = seats.length * seats[0].length;

        if (totalSeats <= LARGE_LIMIT) {
            result = NORMAL_PRICE;
        } else {
            result = row <= seats.length / 2 ? NORMAL_PRICE : CUT_PRICE;
        }

        return result;
    }

    private static void printCinema() {

        int rows = seats.length;
        int cols = seats[0].length;

        System.out.print("\nCinema:\n  ");
        for (int i = 1; i <= cols; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 1; i <= rows; i++) {
            System.out.print(i + " ");
            for (int j = 1; j <= cols; j++) {
                System.out.print(seats[i-1][j-1] + " ");
            }
            System.out.println();
        }
    }

    private static int calculateProfit() {

        int profit;
        int rows = seats.length;
        int cols = seats[0].length;
        int totalSeats = rows * cols;

        if (totalSeats <= LARGE_LIMIT) {
            profit = totalSeats * NORMAL_PRICE;
        } else {
            int firstHalf = rows / 2 * cols * NORMAL_PRICE;
            int lastHalf = (rows - rows / 2) * cols * CUT_PRICE;
            profit = firstHalf + lastHalf;
        }
        return profit;
    }

    public static void createCinema() {

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();
        System.out.println("Enter the number of seats in each row:");
        int cols = scanner.nextInt();

        seats = new char[rows][cols];
        for (char[] row : seats) {
            Arrays.fill(row, 'S');
        }
    }
}